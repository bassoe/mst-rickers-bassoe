import sys

from Graph import Graph


def prim(graph):
    key = [sys.maxsize] * graph.V
    # stored the mst vertixes - set ot all none in the beginning
    parent = [None] * graph.V
    key[0] = 0
    # false - didn't make it to the vertex yet
    reached = [False] * graph.V

    # first node is root, so -1
    parent[0] = -1

    # loop through each vertex
    for t in range(graph.V):

        # Pick vertex that is the least weight away from the vertices (reached) not yet processed.
        u = graph.minKey(key, reached)

        # Put that vertex in the list of reached
        reached[u] = True

        for v in range(graph.V):
            # if graph2[u][v] is nonzero (there is an edge between then in the matrix
            # reached[v] is false - adding the graph will not create a cycle already added to parent
            # Update the key if weight is smaller than previously stored in key[v]
            if graph.graph2[u][v] > 0 and reached[v] == False and key[v] > graph.graph2[u][v]:
                # add to key the keep track of weight to that node
                key[v] = graph.graph2[u][v]
                # add the edge (index of parent) into u
                parent[v] = u
    # create the new graph and return it
    mst = Graph(graph.V)
    for i in range(1, graph.V):
        mst.add_edge(parent[i], i, graph.graph2[parent[i]][i])
    return mst


def kruskal(graph):
    """
    kruskals algorithm to find the mst of the graph
    looks through each edge of the graph, picks the smallest value to
    add the mst that would connect vertex to subgraph of original tree
    as long as it doesn't create a cycle
    (start with edges with the lowest weight and keep adding edges until
    reach the goal
    :param graph: start of the graph data structure
    :return: a new graph that represents the mst
    """
    mst = Graph(len(graph.graph))
    i, e = 0, 0
    # sort the edges by weight (item[2]) so you can start with the smallest weight
    graph.graph = sorted(graph.graph, key=lambda item: item[2])
    parent = []
    rank = []

    # go through each vertex in the graph
    for vertex in range(graph.V):
        # store vertices in parents
        parent.append(vertex)
        # set ranks to zero
        rank.append(0)

    # keeping adding edges to result (representing the mst) until there is one less than
    # the number of vertices because then all vertices are connected once
    while e < graph.V - 1:
        # grab edge from the graph (stores two vertices and weight between them)
        v1, v2, w = graph.graph[i]
        # move onto the next edge
        i = i + 1
        # find edge in parent containing all vertices
        x = graph.find(parent, v1)
        y = graph.find(parent, v2)

        # checks to see if adding this edge will create a cycle
        if x != y:
            # move along to next edge because if it doesn't form a cycle, you can add it
            e = e + 1
            # add edge to result
            mst.add_edge(v1, v2, w)
            # link those two vertices
            graph.union(parent, x, y, rank)

    return mst
