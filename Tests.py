# Test 1: Test with a simple graph with 3 vertices
from Graph import Graph
from MSTSearch import prim
from Main import display_graphs

graph = Graph(3)
graph.add_edge(0, 1, 5)
graph.add_edge(1, 0, 5)
graph.add_edge(2, 1, 3)
graph.add_edge(1, 2, 3)
graph.add_edge(2, 0, 1)
graph.add_edge(0, 2, 1)
display_graphs(graph)
mst = prim(graph)
display_graphs(mst)
print(mst.graph)
assert mst.graph == [[2, 1, 3], [0, 2, 1]]


# Test 3: Test with a graph that has negative weights
graph = Graph(4)
graph.add_edge(0, 1, -1)
graph.add_edge(1, 0, -1)
graph.add_edge(2, 0, 4)
graph.add_edge(0, 2, 4)
graph.add_edge(1, 2, 3)
graph.add_edge(2, 1, 3)
graph.add_edge(1, 3, 2)
graph.add_edge(3, 1, 2)
graph.add_edge(2, 3, -5)
graph.add_edge(3, 2, -5)

display_graphs(graph)
mst = prim(graph)
print(mst.graph)
display_graphs(mst)
assert mst.graph == [[2, 1, 3], [0, 2, 4], [1, 3, 2]]
#
# Test 4: Test with a graph that has a cycle
graph = Graph(4)
graph.add_edge(0, 1, 1)
graph.add_edge(1, 0, 1)
graph.add_edge(2, 1, 2)
graph.add_edge(1, 2, 2)
graph.add_edge(3, 2, 3)
graph.add_edge(2, 3, 3)
graph.add_edge(0, 3, 4)
graph.add_edge(3, 0, 4)
display_graphs(graph)
mst = prim(graph)
print(mst.graph)
display_graphs(mst)
assert mst.graph == [[0, 1, 1], [1, 2, 2], [2, 3, 3]]

# Test 5: Test with a large graph
graph = Graph(100)
for i in range(99):
    graph.add_edge(i, i + 1, 1)
display_graphs(graph)
mst = prim(graph)
print(mst.graph)
print([[i, i + 1, 1] for i in range(99)])

assert mst.graph == [[i, i + 1, 1] for i in range(99)]