# Final Project

#### Authors: Eden Basso and Sofia Ricker 

## Project Description

This project contains the algorithms Prim's and Kruskal's in order to find the minimum spanning tree.

## Running project
You should be able to simple run using Main.py.

The first prompt will be which graph you wish to use, contained in the graph_files directory. Simply enter the 
name of the file inside that directory, for example sample_graph.

Next, the program will prompt you to enter which algorithm, enter p for prims and k for kruskals (if you enter enything other 
than k or K, the program will run with prims)

Next it will prompt you to y - yes or n - no if you want to see the visualization or not.
(not anything other than y will result in no visualization). The first graph displayed will be 
the original tree. After you close out of that display, the second one will be the mst.
Output in the console is the memory usage and runtime.


## Libraries

- matplotlib - part of the display of the graph
- networkx - displays the trees
https://networkx.org/documentation/stable/auto_examples/drawing/plot_weighted_graph.html