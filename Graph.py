from __future__ import annotations

import sys


class Graph:
    """
    Represents the graph and tree for the project
    Represents an adjacency matrix by keeping track of the edges through
    a tuple of (vertex1, vertex2, weight)
    """
    def __init__(self, v):
        """
        constructor
        takes in the number of vertices
        """
        self.V = v
        self.graph = []
        # create matrix version 0 - no edge
        self.graph2 = [[0 for column in range(v)]
                      for row in range(v)]

    def add_edge(self, v1, v2, w):
        """
        adds edges that connect the two vertexes together
        specific data structure using union-find data structure
        Explanation of data structure: https://people.cs.umass.edu/~barring/cs611/lecture/7.pdf
        :param v1: the first vertex
        :param v2: the second vertex
        :param w: the weight of the edge
        :return: none
        """
        self.graph.append([v1, v2, w])
        self.graph2[v1][v2] = w

    def find(self, parent, i):
        """
        recursively search to see if there is an edge/ connection between
        :param parent: a list of all the vertices
        :param i: the vertex you want to find
        :return: vertex if in the list
        """
        if parent[i] == i:
            return i
        return self.find(parent, parent[i])

    def union(self, parent, v1, v2, rank):
        """
        this checks if two vertices (v1 and v2) are in the same set/ cluster
        in order to verify that if adding that edge will create a cycle or not,
        if it doesn't create a cycle, links subsets repsented by vertexes v1 and v2
        :param parent: list of vertexes
        :param v1: vertex one
        :param v2: vertex two
        :param rank: weights added to mst
        :return: none
        """
        a = self.find(parent, v1)
        b = self.find(parent, v2)
        if rank[a] < rank[b]:
            parent[a] = b
        elif rank[a] > rank[b]:
            parent[b] = a
        else:
            parent[b] = a
            rank[a] += 1

    def minKey(self, key, reached):
        # min value to something to be overrun
        min = sys.maxsize
        # loop thoughout each vertex
        for v in range(self.V):
            # if not reached before and smallest weight
            if key[v] < min and reached[v] == False:
                min = key[v]
                min_index = v
        # return that vertex
        return min_index





