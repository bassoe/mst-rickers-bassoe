import networkx as nx
import matplotlib.pyplot as plt
from Graph import Graph
from MSTSearch import kruskal
from MSTSearch import prim
import tracemalloc
import time


def main():
    # f = open("graph_files/sparse5", "a")
    # #f.write("1000 \n")
    # for i in range(999):
    #     f.write(str(i+1) + ", " + str(i) + ", 1 \n")
    #     i += 1
    #
    # f.close()

    file_path = "graph_files/"
    name = input("Enter a graph to find the MST of: ")
    alg = input("Enter the algorithm you want to use, p)rim or k)ruskals :")
    g = input("Display graph: y or n: ")
    try:
        graph = read_graph(file_path + name)
        if g == "y":
            display_graphs(graph)
        stats = []
        if alg == 'k' or alg == 'K':
            start = time.time()
            tracemalloc.start()
            mst = kruskal(graph)
            memory_usage = tracemalloc.get_traced_memory()
            stats.append(memory_usage[1])
            tracemalloc.stop()
            end = time.time()
            stats.append(end - start)
        else:
            start = time.time()
            tracemalloc.start()
            mst = prim(graph)
            memory_usage = tracemalloc.get_traced_memory()
            stats.append(memory_usage[1])
            tracemalloc.stop()
            end = time.time()
            stats.append(end - start)
        print("Memory Usage: ", stats[0])
        print("Run time: ", stats[1])
        if g == "y":
            display_graphs(mst)
    except Exception as e: print(str(e))


def read_graph(file_path):
    f = open(file_path, "r")
    graph = Graph(int(f.readline()))
    for line in f:
        nums = line.split(',')
        graph.add_edge(int(nums[0]), int(nums[1]), int(nums[2]))
    f.close()
    return graph


def display_graphs(graph):
    G = nx.Graph()
    for edge in graph.graph:
        G.add_edge(edge[0], edge[1], weight=edge[2])

    pos = nx.spring_layout(G, seed=7)  # positions for all nodes - seed for reproducibility

    # second one will appear after you close the previous one
    nx.draw_networkx_nodes(G, pos, node_size=700)

    # edges
    nx.draw_networkx_edges(G, pos, width=6)
    nx.draw_networkx_labels(G, pos, font_size=20, font_family="sans-serif")
    # edge weight labels
    edge_labels = nx.get_edge_attributes(G, "weight")
    nx.draw_networkx_edge_labels(G, pos, edge_labels)

    ax = plt.gca()
    ax.margins(0.08)
    plt.axis("off")
    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()
